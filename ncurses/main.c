#include <ncurses.h>

int main(int argc, char **argv){
    initscr();
    int c;
    while((c=getch()) != 27){
        move(0,0);
        printw("Keycode: %d",c);
        move(1,0);
        printw("Char: %c",c);
        refresh();
    }
    endwin();
    return 0;
}