#include <stdio.h>
#include "setterProto.h"
#include "topb.h"
#include "turn.h"
#include "turnDecider.h"
#include "winerSetter.h"
#include "clear.h"
#include "vars.h"

char marks[3] = {'X', 'O', ' '};
unsigned int quadrants[9] = {2,2,2,2,2,2,2,2,2};
char* quadrantsMap = "1|2|3\n-|-|-\n4|5|6\n-|-|-\n7|8|9";
unsigned int turnInt;

int main(int argc, char** argv){
    printf("Welcome to the Old Lady Game\n");
    /*char marks[2] = {'X', 'O'};
    uint quadrants[9] = {2,2,2,2,2,2,2,2,2};*/
    /*Board:
    O|O|O
    -|-|-
    X|X|X
    -|-|-
    O|O|O
    */
    turnDecider();
    while(1){
        setterProto();
        clear();
        terminalOutPutBuffer();
        winerSetter();
        turn();
    }
    return 0;
}