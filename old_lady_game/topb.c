/*
topb is a acronym for terminalOutPutBuffer
*/
#include <stdio.h>
#include "topb.h"
#include "vars.h"

// marks[3] = {'X', 'O', ' '};
// quadrants[9] = {2,2,2,2,2,2,2,2,2};
// char marks[3];
// unsigned int quadrants[9];

void terminalOutPutBuffer(void){
    printf("%c|%c|%c\n-|-|-\n%c|%c|%c\n-|-|-\n%c|%c|%c\n",
    marks[quadrants[0]],marks[quadrants[1]],marks[quadrants[2]],marks[quadrants[3]],
    marks[quadrants[4]],marks[quadrants[5]],marks[quadrants[6]],marks[quadrants[7]],
    marks[quadrants[8]]);
}