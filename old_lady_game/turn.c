#include <stdio.h>
#include "turn.h"
#include "vars.h"

void turn(void){
    switch (turnInt) {
    case 1:
            turnInt = 0;
            break;
    case 0:
            turnInt = 1;
            break;
    default:
            printf("Error value out of scope!\n");
            break;
    }
    printf("It's %c turn now!\n",marks[turnInt]);
}