#include <stdio.h>
#include <stdlib.h>
#include "vars.h"
#include "winerSetter.h"

// unsigned int quadrants[9] = {2,2,2,2,2,2,2,2,2};

/*
[0][1][2]
[3][4][5]
[6][7][8]
*/

void winerSetter(){
    // for X
    if(
    // horizontal X
    quadrants[0]==0 && quadrants[1]==0 && quadrants[2]==0 ||
    quadrants[3]==0 && quadrants[4]==0 && quadrants[5]==0 ||
    quadrants[6]==0 && quadrants[7]==0 && quadrants[8]==0 ||
    // vertical X
    quadrants[0]==0 && quadrants[3]==0 && quadrants[6]==0 ||
    quadrants[1]==0 && quadrants[4]==0 && quadrants[7]==0 ||
    quadrants[2]==0 && quadrants[5]==0 && quadrants[8]==0 ||
    // diagonal X
    quadrants[0]==0 && quadrants[4]==0 && quadrants[8]==0 ||
    quadrants[2]==0 && quadrants[4]==0 && quadrants[6]==0
    ){
        printf("X wins!\n");
        exit(0);
    }
    //for O
    else if(
    // horizontal O
    quadrants[0]==1 && quadrants[1]==1 && quadrants[2]==1 ||
    quadrants[3]==1 && quadrants[4]==1 && quadrants[5]==1 ||
    quadrants[6]==1 && quadrants[7]==1 && quadrants[8]==1 ||
    // vertical O
    quadrants[0]==1 && quadrants[3]==1 && quadrants[6]==1 ||
    quadrants[1]==1 && quadrants[4]==1 && quadrants[7]==1 ||
    quadrants[2]==1 && quadrants[5]==1 && quadrants[8]==1 ||
    // diagonal O
    quadrants[0]==1 && quadrants[4]==1 && quadrants[8]==1 ||
    quadrants[2]==1 && quadrants[4]==1 && quadrants[6]==1
    ){
        printf("O wins!\n");
        exit(0);
    }
}