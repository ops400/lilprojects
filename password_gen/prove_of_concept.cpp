#include <cstdlib>
#include <iostream>

#define MAX_RANDOM 75

using std::cout;
using std::cin;

int main(int argc, char **argv) {
    int passwordLength;
    char multipleCharacters[76] = {
        '!', '"', '#',  '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-',
        '.', '/', '\\', ':', ';', '<', '=',  '?', '@', '[', ']', 'a', 'b',
        'c', 'd', 'e',  'f', 'g', 'h', 'i',  'j', 'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r',  's', 't', 'u', 'v',  'w', 'x', 'y', 'z', 'A', 'B', 
        'C', 'D', 'E',  'F', 'G', 'H', 'I',  'J', 'K', 'L', 'M', 'N', 'O', 
        'P', 'Q', 'R',  'S', 'T', 'U', 'V',  'W', 'X', 'Y', 'Z'};
    cout << "What is the desired password length?\nPlease insert here:";
    cin >> passwordLength;
    srand(time(0));
    for(int i = 0;i < passwordLength;i++){
        cout << multipleCharacters[rand()%MAX_RANDOM];
    }
    cout << '\n';
}