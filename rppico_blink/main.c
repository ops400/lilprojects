#include "pico/stdlib.h"
#define LED_EXTERN_PIN  0

int main(){
    gpio_init(LED_EXTERN_PIN);
    gpio_set_dir(LED_EXTERN_PIN, GPIO_OUT);
    while(1){
        gpio_put(LED_EXTERN_PIN, 1);
        sleep_ms(500);
        gpio_put(LED_EXTERN_PIN, 0);
        sleep_ms(500);
    }
}