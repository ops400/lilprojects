#ifndef GENERALDEFS_H
#define GENERALDEFS_H

#ifdef MAINFILE
	#define EXT
#else
	#define EXT extern
#endif

void input(void);
void prgLoop(void);
EXT int Er;
EXT int Eg;
EXT int Eb;

#endif
