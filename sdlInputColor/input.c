#include <SDL2/SDL.h>
#include <stdlib.h>
#include "generalDefs.h"

void input(void){
	SDL_Event event;
	while(SDL_PollEvent(&event)){
		switch(event.type){
			case SDL_KEYDOWN:
				if(event.key.keysym.sym == SDLK_ESCAPE){
					exit(1);
				}
				if(event.key.keysym.sym == SDLK_r){
					if(Er != 255){
						Er++;
					}
					else{
						Er = 0;
					}
				}
				if(event.key.keysym.sym == SDLK_g){
					if(Eg != 255){
						Eg++;
					}
					else{
						Eg = 0;
					}
				}
				if(event.key.keysym.sym == SDLK_b){
					if(Eb != 255){
						Eb++;
					}
					else{
						Eb = 0;
					}
				}
		}
	}
}
