#include <SDL2/SDL.h>
#include "prgSetup.h"
#include "generalDefs.h"

void prgLoop(void){
	sceneSetup();
	input();
	sceneShow();
	SDL_Delay(16);
}
