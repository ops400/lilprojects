#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "prgSetup.h"
#define SWPU SDL_WINDOWPOS_UNDEFINED

void prgSetup(int width, int height, char* windowName){
	int renderFlags;
	int windowFlags;
	renderFlags = SDL_RENDERER_ACCELERATED;
	windowFlags = 0;
	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		printf("Error at SDL_Init\n");
		exit(1);
	}
	window = SDL_CreateWindow(windowName, SWPU, SWPU, width, height, windowFlags);
	if(!window){
		printf("Error at window\n");
		exit(1);
	}
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
	renderer = SDL_CreateRenderer(window, -1, renderFlags);
	if(!renderer){
		printf("Error at renderer\n");
		exit(1);
	}

}
