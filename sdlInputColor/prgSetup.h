#ifndef PRGSETUP_H
#define PRGSETUP_H
#include <SDL2/SDL.h>

#ifdef MAINFILE
	#define EXT
#else
	#define EXT extern
#endif

EXT SDL_Renderer* renderer;
EXT SDL_Window* window;

void prgSetup(int width, int height, char* windowName);
void sceneSetup(void);
void sceneShow(void);

#endif
