#include <SDL2/SDL.h>
#include "generalDefs.h"
#include "prgSetup.h"

void sceneSetup(void){
	SDL_SetRenderDrawColor(renderer, Er, Eg, Eb, 255);
	SDL_RenderClear(renderer);
	
}

void sceneShow(void){
	SDL_RenderPresent(renderer);
}
