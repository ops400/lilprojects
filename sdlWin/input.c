#include <SDL2/SDL.h>
#include <stdlib.h>
#include "generalDefs.h"

int input(){
	SDL_Event event;
	
	while(SDL_PollEvent(&event)){
		switch(event.type){
			case SDL_KEYDOWN:
				if(event.key.keysym.sym == SDLK_ESCAPE){
					exit(0);
				}
				break;
			default:
				break;
		}
	}
}
