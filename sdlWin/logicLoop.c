#include <SDL2/SDL.h>
#include "window.h"
#include "generalDefs.h"

void logicLoop(){
	drawOnWindow();
	input();
	showScene();
	SDL_Delay(16);
}
