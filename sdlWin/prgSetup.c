#include <SDL2/SDL.h>
#include <stdio.h>
#include "window.h"

int SDLWPU = SDL_WINDOWPOS_UNDEFINED;

int prgSetup(int width, int height, char* windowName){
	int rendererFlags;
	int windowFlags;
	rendererFlags = SDL_RENDERER_ACCELERATED;
	windowFlags = 0;

	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		printf("Faild at SDL_Init\n");
		return 1;
	}

	window =  SDL_CreateWindow(windowName, SDLWPU, SDLWPU, width, height, windowFlags);

	if(!window){
		printf("Faild at SDL_Window\n");
		return 1;
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
	renderer = SDL_CreateRenderer(window, -1, rendererFlags);

	if(!renderer){
		printf("Faild at SDL_Renderer\n");
		return 1;
	}
}
