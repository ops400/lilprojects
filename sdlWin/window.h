#ifndef WINDOW_H
#define WINDOW_H
#include <SDL2/SDL.h>

#ifdef MAINFILE
	#define EXTERN
#else 
	#define EXTERN extern
#endif

EXTERN SDL_Window* window;
EXTERN SDL_Renderer* renderer;

int prgSetup(int width, int height, char* windowName);
void drawOnWindow(void);
void showScene(void);

#endif
